#!/bin/bash
set -eu
function usage() {
    echo "usage: $1 <basefile> [<arch>]"
    echo "\t<basefile> of the form trace-perf.YYYYMMDDHHMM"
    echo "\t<arch>\t[intel|adm], default: intel"
}
if [ $# -lt 1 ]
then
    usage $0 >&2
    exit 1
fi
# First argument
basename="$1"
case "$basename" in
    trace-perf.????????????.bw|trace-perf.????????????.lat)
        basename="${basename%.lat}"
        basename="${basename%.bw}"
        ;;
    trace-perf.????????????)
        ;;
    *)
        usage $0 >&2
        exit 2
        ;;
esac
basedate="${basename#trace-perf.}"
# Second argument
arch="${2:-intel}"
case "$arch" in
    intel)
        nodeseq="`seq 0 3`"
        bwseq="`seq 10 10 100`"
        ntp="`echo -e "%\n\t0/DRAM (NUMA\\#\\numa)/north east,%\n\t2/cross-NUMA DRAM (NUMA\\#\\numa)/north east,%\n\t1/NVDIMM (NUMA\\#\\numa)/south east,%\n\t3/cross-NUMA NVDIMM (NUMA\\#\\numa)/south east%\n"`"
        ymax=430
        legendcol=2
        bwsentries='$BW=10$,$BW=20$,$BW=30$,$BW=40$,$BW=50$,$BW=60$,$BW=70$,$BW=80$,$BW=90$,$BW=100$'
        bwslist='10,20,...,100'
        merged=1
        numalist='0,2,1,3'
        extralabels="`echo -e "\\ifnum\\c=1 \\node [anchor=north east,font=\\footnotesize] at (endof) {local DRAM};\\else \\fi\n\t\t\\ifnum\\c=2 \\node [anchor=north east,font=\\footnotesize] at (endof) {cross-NUMA DRAM};\\else \\fi\n\t\t\\ifnum\\c=3 \\node [anchor=north east,font=\\footnotesize] at (endof) {local NVDIMM};\\else \\fi\n\t\t\\ifnum\\c=4 \\node [anchor=north east,font=\\footnotesize] at (endof) {cross-NUMA NVDIMM};\\else \\fi\n"`"
        ;;
    amd)
        arch="AMD"
        nodeseq="`seq 0 1`"
        bwseq="`seq 9`" #1 `seq 10 10 100`" #1105 206 404 566 793 1111 1300 1555 1750 2048"
        ntp="`echo -e "%\n\t0/DRAM (package local)/outer north east,%\n\t1/DRAM (cross package)/outer north east%\n\t"`"
        ymax=420
        legendcol=1
        bwsentries="`seq 9`" #1 `seq 10 10 100`" #105 206 404 566 793 1111 1300 1555 1750 2048"
        bwslist='1,2,...,9' #10,20,...,100' #105,206,404,566,793,1111,1300,1555,1750,2048'
        merged=0
        numalist='0,1'
        coreseq="`seq 0 1`"
        corelist='0,1'
        extralabels="`echo -e "\\ifnum\\c=1 \\node [anchor=north east,font=\\footnotesize] at (endof) {local DRAM};\\else \\fi\n\t\t\\ifnum\\c=2 \\node [anchor=north east,font=\\footnotesize] at (endof) {cross-NUMA DRAM};\\else \\fi\n"`"
        ;;
    *)
        usage $0 >&2
        exit 1
        ;;
esac
legendentries="`for bw in $bwsentries; do echo -n "\\ensuremath{BW=$bw},"; done`"
if [ -d "$basename" ]
then
    pushd "$basename"
    # latency
    latbasename="$basename.lat"
    latencytex="../latency.${basedate}.tex"
    for numa in $nodeseq
    do
        for bw in $bwseq
        do
            for core in $coreseq
            do
                grep "^$bw\s" "$latbasename.$numa.out" | \
                    grep "$core$" | \
                    sed -e "s/^$bw\s\+//" -e 's/\s\+/ /g' | \
                    cut -d' ' -f1,9 | \
                    sort -n > "$latbasename.$core.$numa.$bw.out"
            done
        done
    done
    cat >"$latencytex" <<EOF
\documentclass{standalone}
\usepackage{tikz,xcolor,subfigure,float,graphicx}
\usepackage{listings}
\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background,main,foreground}
\usetikzlibrary{calc,positioning,arrows.meta,intersections,fit,positioning,shapes,calc}
\usetikzlibrary{shapes.geometric,shapes.arrows,patterns,patterns.meta,matrix}
\usepackage{amsmath}
\usepackage{pgfplots,pgffor}
\usepgfplotslibrary{dateplot} % LATEX and plain TEX
\pgfplotsset{compat=1.17}
\begin{document}
\foreach \bw in {$bwslist}{
    \foreach \numa/\title/\position in {$ntp}{
        \begin{tikzpicture}
            \begin{axis}[
                title={Latency to core 0 and 1 from \\title --- arch: $arch},
                xmin=0,
                ymin=0,
                ymax=$ymax,
                xtick=data,
                xlabel=\# threads,
                ylabel=Avg latency (ns),
                legend entries={$legendentries},
                legend columns=$legendcol,
                legend pos={\position},
                ]
                \foreach \core in {$corelist}{
                    \addplot table {./$basename/$latbasename.\core.\numa.\bw.out};
                }
            \end{axis}
        \end{tikzpicture}
        \bigskip
    }
}
EOF
    if [ 0 -lt ${merged:-0} ]
    then
        cat >>"$latencytex" <<EOF
\begin{tikzpicture}
    \begin{axis}[
        title=Latency to cores in single package --- arch: $arch,
        xmin=0,
        ymin=0,
        xtick=data,
        xlabel=\# threads,
        ylabel=Avg latency (ns),
        legend entries={$legendentries},
        legend columns=1,
        legend pos=outer north east,
        ]
        \foreach \numa [count=\c] in {$numalist}%
        {
            \foreach \bw in {$bwslist}{
                \addplot table {./$basename/$latbasename.\numa.\bw.out} node [below,outer sep=.3\baselineskip] (endof) {};%
            }
            $extralabels
        }
    \end{axis}
\end{tikzpicture}
EOF
    fi
    echo "\end{document}" >>"$latencytex"
    # bw
    bwbasename="$basename.bw"
    bwtex="../bandwidth.${basedate}.tex"
    for numa in $nodeseq
    do
        for bw in $bwseq
        do
            for op in copy scale add triad
            do
                for core in $coreseq
                do
                    grep "^$bw\s" "$bwbasename.$numa.out" | \
                        grep "$core$" | \
                        grep -i $op | \
                        sed -e "s/^$bw\s\+//" -e 's/\s\+/ /g' | \
                        cut -d' ' -f1,3 | \
                        sort -n > "$bwbasename.$numa.$bw.$op.out"
                done
            done
        done
    done
    cat >"$bwtex" <<EOF
\documentclass[12pt,a4paper,varwidth]{standalone}
\usepackage{tikz,xcolor,subfigure,float,graphicx}
\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background,main,foreground}
\usetikzlibrary{calc,positioning,arrows.meta,intersections,fit,positioning,shapes,calc}
\usetikzlibrary{shapes.geometric,shapes.arrows,patterns,patterns.meta,matrix}
\usepackage{amsmath}
\usepackage{pgfplots,pgffor}
\usepgfplotslibrary{dateplot} % LATEX and plain TEX
\pgfplotsset{compat=1.17}
\begin{document}
                 \foreach \bw in {$bwslist}
\foreach \numa/\title/\position in {$ntp}{
    \foreach \op in {copy,scale,add,triad}{%
         \begin{tikzpicture}
             \begin{axis}[
                 align=center,
                 title={Bandwidth for \op{} operation to cores in package 0 from\\\\\\title --- arch: $arch},
                 xmin=0,
                 ymin=0,
                 xtick=data,
                 xlabel=\# threads,
                 ylabel=Best Rate MB/s,
                 scaled ticks=false,
                 tick label style={/pgf/number format/fixed},
                 legend entries={$legendentries},
                 legend columns=1,
                 legend pos={\position}
                 ]
                \foreach \core in {$corelist}{
                     \addplot table {./$basename/$bwbasename.\core.\numa.\bw.\op.out};
             \end{axis}
         \end{tikzpicture}
     }
     \par
}
}
\end{document}
EOF
    popd
fi
