Diana is a simple multi-threaded pointer chasing application.

Usage:

```
./diana [-s <size> | -t <stride> | -S <seed> | -i <niter> | -c <nchasing> | -v | --header-only | --with-header | --without-header]
```

Where:
 - `<size>` buffer size; if not a factor of `sizeof(void*)` the remainer is ignored.
 - `<stride>` stride between pointers (must be >= `sizeof(void*)`).
 - `<niter>` number of iterations.
 - `<nchasing>` number of pointer chasing iterations.
 - `-v` to make increase the application verbosity.
 - `--header-only` prints the result header and exits.
 - `--with-header` default case, print the result table with its header.
 - `--without-header` print the result table without its header.
