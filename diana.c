#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <pthread.h>

#define KiB(n) (((size_t)(n))<<10)
#define MiB(n) (KiB(n)<<10)
#define GiB(n) (MiB(n)<<10)

static void usage(const char *name, FILE* out)
{
  fprintf(out, "usage: %s [-s <size> | -t <stride> | -S <seed> | -i <niter> | -c <nchasing> | -v | --header-only | --with-header | --without-header]\n"
               "\t<size>          \tbuffer size; if not a factor of %zu, "
               "the remainer is ignored.\n"
               "\t<stride>        \tstride between pointers (must be >= %zu).\n"
               "\t<niter>         \tnumber of iterations.\n"
               "\t<nchasing>      \tnumber of pointer chasing iterations.\n"
               "\t-v              \tto make increase the application verbosity\n"
               "\t--header-only   \tprints the result header and exits\n"
               "\t--with-header   \tdefault case, print the result table with its header\n"
               "\t--without-header\tprint the result table without its header.\n",
          name, sizeof(void*), sizeof(void*));
}

static inline void get_time(struct timespec *t)
{
#if defined(__bg__)
#  define CLOCK_TYPE CLOCK_REALTIME
#elif defined(CLOCK_MONOTONIC_RAW)
#  define CLOCK_TYPE CLOCK_MONOTONIC_RAW
#else
#  define CLOCK_TYPE CLOCK_MONOTONIC
#endif
  clock_gettime(CLOCK_TYPE, t);
#undef CLOCK_TYPE
}

static inline double time_diff(const struct timespec *const t1, const struct timespec *const t2)
{
  const double diff = 1000000000.0 * (t2->tv_sec - t1->tv_sec) + (t2->tv_nsec - t1->tv_nsec);
  return diff;
}

static int comp_double(const void*_a, const void*_b)
{
  const double* a = _a;
  const double* b = _b;
  if(*a < *b)
    return -1;
  else if(*a > *b)
    return 1;
  else
    return 0;
}

static const char *tab_header = "# jumps \t| min    \t | median  \t| q1     \t| q3     \t| d1     \t| d9     \t| avg    \t| max";
static int print_result(double *const times, size_t niter, size_t njmp, int with_header)
{
  int ret = 0;
  qsort(times, niter, sizeof(double), &comp_double);
  const double min_time = times[0];
  const double max_time = times[niter - 1];
  const double med_time = times[(niter - 1) / 2];
  const double q1_time  = times[(niter - 1) / 4];
  const double q3_time  = times[ 3 *(niter - 1) / 4];
  const double d1_time  = times[(niter - 1) / 10];
  const double d9_time  = times[ 9 *(niter - 1) / 10];
  double avg_time = 0.0;
  for (size_t i = 0; i < niter; i++) {
    avg_time += times[i];
  }
  avg_time /= niter;
  if (with_header)
    ret += printf("%s\n", tab_header);
  return ret + printf("%9zu\t%9.3lf\t%9.3lf\t%9.3lf\t%9.3lf\t%9.3lf\t%9.3lf\t%9.3lf\t%9.3lf\n",
      njmp, min_time/(double)njmp, med_time/(double)njmp, q1_time/(double)njmp,
      q3_time/(double)njmp, d1_time/(double)njmp, d9_time/(double)njmp,
      avg_time/(double)njmp, max_time/(double)njmp);
}

static size_t pick_rand(size_t range)
{
  size_t rnd = RAND_MAX;
  const size_t rnd_max = rnd - rnd % range;
  while (rnd_max <= rnd)
    rnd = rand();
  return rnd % range;
}

typedef struct {
  void **ptr;
  double *times;
  size_t niter, nchasing;
} thread_data_t;

void *run_bench(void *params)
{
  thread_data_t *thread_data = params;
  void **ptr = thread_data->ptr;
  /* Run benchmark */
  struct timespec begin, end;
  for (size_t i = 0; thread_data->niter > i; ++i) {
    /* pointer chasing */
    get_time(&begin);
    for (size_t step = 0; thread_data->nchasing > step; ++step)
      ptr = *ptr;
    get_time(&end);
    /* escape function from https://www.youtube.com/watch?v=nXaxk27zwlk */
    asm volatile("" : : "g"(ptr) : "memory");
    thread_data->times[i] += time_diff(&begin, &end);
  }
  return NULL;
}

int main (int argc, char **argv)
{
  intmax_t seed = time(NULL);
  size_t size = GiB(sizeof(void*));
  size_t niter = 20;
  size_t nchasing = 1UL<<23;
  size_t stride = sizeof(void*);
  size_t nthreads = 1;
  int verbose = 0;
  int with_header = 1;
  for (char **arg = &argv[1]; &argv[argc] > arg; ++arg) {
#define CHECK_ARGV(arg, argc, argv) \
    do {                            \
      if (&argv[argc] <= arg) {     \
        usage(argv[0], stderr);     \
        return EXIT_FAILURE;        \
      }                             \
    } while (0)
    if (0 == strncmp("-S", *arg, 2)) {
      arg += 1;
      CHECK_ARGV(arg, argc, argv);
      sscanf(*arg, "%lu", &seed);
    } else if (0 == strncmp("-s", *arg, 2)) {
      arg += 1;
      CHECK_ARGV(arg, argc, argv);
      sscanf(*arg, "%lu", &size);
    } else if (0 == strncmp("-t", *arg, 2)) {
      arg += 1;
      CHECK_ARGV(arg, argc, argv);
      sscanf(*arg, "%lu", &stride);
    } else if (0 == strncmp("-T", *arg, 2)) {
      arg += 1;
      CHECK_ARGV(arg, argc, argv);
      sscanf(*arg, "%lu", &nthreads);
    } else if (0 == strncmp("-i", *arg, 2)) {
      arg += 1;
      CHECK_ARGV(arg, argc, argv);
      sscanf(*arg, "%zu", &niter);
    } else if (0 == strncmp("-c", *arg, 2)) {
      arg += 1;
      CHECK_ARGV(arg, argc, argv);
      sscanf(*arg, "%zu", &nchasing);
    } else if (0 == strncmp("-v", *arg, 2)) {
        verbose = 1;
    } else if (0 == strncmp("--with-header", *arg, strlen("--with-header"))) {
        with_header = 1;
    } else if (0 == strncmp("--without-header", *arg, strlen("--without-header"))) {
        with_header = 0;
    } else if (0 == strncmp("--header-only", *arg, strlen("--header-only"))) {
        printf("%s\n", tab_header);
        return EXIT_SUCCESS;
    } else if (0 == strncmp("--help", *arg, 6) || 0 == strncmp("-h", *arg, 2)) {
      usage(argv[0], stdout);
      return EXIT_SUCCESS;
    } else {
      usage(argv[0], stderr);
      return EXIT_FAILURE;
    }
#undef CHECK_ARGV
  }
  size /= stride;
  if (0 == size || 0 == stride || sizeof(void*) > stride || 0 == nthreads) {
    usage(argv[0], stderr);
    return EXIT_FAILURE;
  }
  srand(seed);
  if (verbose)
    printf("configuration: %zu entries (stride: %zub), seed: %zu, "
        "%zu chasing, %zu iterations, %zu threads\n",
        size, stride, seed, nchasing, niter, nthreads);
  if (verbose) {
    printf("Generating buffer...");
    fflush(stdout);
  }
  unsigned char *buff = malloc(size * stride);
  if (NULL == buff) {
    fprintf(stderr, "Unable to allocate buff array.\n");
    return EXIT_FAILURE;
  }
  double *times = malloc(sizeof(double[niter * nthreads]));
  if (NULL == times) {
    fprintf(stderr, "Unable to allocate timing array.\n");
    free(buff);
    return EXIT_FAILURE;
  }
  /* Fill buffer and swap randomly the entries */
  for (size_t i = 0; size > i; ++i) {
    void **ptr = (void **)(buff + i * stride);
    *ptr = ptr;
  }
  for (size_t i = 0; size - 1 > i; ++i) {
    size_t chosen_index = pick_rand(size-i-1);
    void **p1 = (void **)(buff + chosen_index * stride);
    void **p2 = (void **)(buff + stride * (size-i-1));
    void *tmp = *p1;
    *p1 = *p2;
    *p2 = tmp;
  }
  if (verbose) {
    printf(" Done.\nStarting pointer chasing...");
    fflush(stdout);
  }
  /* Initialisation of the per thread data */
  thread_data_t thread_data[nthreads];
  for (size_t i = 0; nthreads > i; ++i) {
    thread_data[i].ptr = (void **)(buff + stride * pick_rand(size));
    thread_data[i].times = &times[niter * i];
    thread_data[i].niter = niter;
    thread_data[i].nchasing = nchasing;
  }
  /* Run benchmark */
  pthread_t threads[nthreads];
  for (size_t i = 0; nthreads > i; ++i)
    pthread_create(&threads[i], NULL, run_bench, &thread_data[i]);
  for (size_t i = 0; nthreads > i; ++i)
    pthread_join(threads[i], NULL);
  if (verbose)
    printf(" Done.\n");
  print_result(times, niter * nthreads, nchasing, with_header);
  free(buff);
  free(times);
  return EXIT_SUCCESS;
}
