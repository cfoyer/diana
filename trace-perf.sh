#!/bin/bash
set -eu
function usage() {
    echo "usage: $1 [<arch>]"
    echo "\t<arch>\t[intel|adm], default: intel"
}
if [ $USER != root ]
then
    echo "Script must be launched as root.\n" >&2
    exit 1
fi
arch="${1:-intel}"
case "$arch" in
    intel)
        nodeseq="`seq 0 3`"
        bwseq="`seq 10 10 100`"
        schemseq="0"
        thseq="1 2 `seq 4 4 20`"
        ;;
    amd)
        nodeseq="`seq 0 1`"
        bwseq="1 105 206 404 566 793 1111 1300 1555 1750 2048"
        schemseq="`seq 0 7`"
        thseq="1 2 `seq 4 4 64`"
        ;;
    *)
        usage $0 >&2
        exit 1
        ;;
esac

## bw modification
schem_file= # path to /sys/fs/Resctrl/<control rule dir>
## latency trace
lat_bin= # path to diana
#-v -c 1000000 -i 100 -s $((2**26)) -t 128 -T 1
nchase=1000000
niter=100
datasize=$((2**26))
stride=128
lat_cmd="$lat_bin ${verbose:+-v} --without-header -c $nchase -i $niter -s $datasize -t $stride -T"
## bandwidth trace
bw_bin= # path to STREAM
bw_cmd="$bw_bin --quiet"
## hwloc-bind
# hwloc-bind --membind node:$i --cpubind core:0-3
outfile=./trace-perf.`date +%Y%m%d%H%M`.out
echo "outfile: ${outfile/.out/.[lat|bw].out}"
for node in $nodeseq
do
    # Latency
    $lat_bin --header-only | sed -e 's/^/schemata\t| nthreads\t|/'| tee ${outfile/.out/.lat.$node.out}
    for bw in $bwseq
    do
        for numa in $schemseq
        do
            echo "MB:$numa=$bw;" > $schem_file
        done
        for nth in $thseq
        do
            hwloc-bind --membind node:$node --cpubind package:0 -- $lat_cmd $nth | \
                sed -e "s/^\s\+$nchase/$bw\t   $nth       \t&/" | \
                tee -a ${outfile/.out/.lat.$node.out}
        done
    done
    # BW
    $bw_bin --header | sed -e 's/^/schemata\t  nthreads\t /'| tee ${outfile/.out/.bw.$node.out}
    for bw in $bwseq
    do
        for numa in $schemseq
        do
            echo "MB:$numa=$bw;" > $schem_file
        done
        for nth in $thseq
        do
            OMP_NUM_THREADS=$nth hwloc-bind --membind node:$node --cpubind package:0 -- $bw_cmd | \
                sed -e "s/^/$bw\t   $nth       \t&/" | \
                tee -a ${outfile/.out/.bw.$node.out}
        done
    done
done
# chown <username> ${outfile/.out/.*.out}
